"""

"""

# import python module
import time
import numpy as np
def generate_int_matrix(m=3, n=4):
    """
    Generate random int matrix, number range in(1, 8)
    """
    matrix = np.random.randint(0, 5, (m, n))
    return matrix

def multi_int_matrices():
    matrix1 = generate_int_matrix(m=300, n=400)
    matrix2 = generate_int_matrix(m=400, n=300)
    start = time.time()
    result = np.dot(matrix1, matrix2)
    end = time.time()
    return result, end - start
if __name__ == '__main__':
    result1, time1 = multi_int_matrices()
    print(time1)

