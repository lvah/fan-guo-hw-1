import random
import time


def generate_float_matrix(m=3, n=4):
    """
    Generate random int matrix, number range in(1, 8)
    """
    matrix = []
    for i in range(m):
        item = [random.uniform(0, 2) for j in range(n)]
        matrix.append(item)
    return matrix


def multi_float_matrices():
    row1, column1 = 300, 400
    row2, column2 = 400, 300
    matrix1 = generate_float_matrix(m=row1, n=column1)
    matrix2 = generate_float_matrix(m=row2, n=column2)
    result = [[0] * column2 for i in range(row1)]
    start = time.time()
    for i in range(row1):
        # i是矩阵的
        for j in range(column2):
            for k in range(row2):
                temp = matrix1[i][k] * matrix2[k][j]
                result[i][j] *= temp
    end = time.time()
    return result, end - start


if __name__ == '__main__':
    result, time = multi_float_matrices()
    # print(result)
    print(time)
