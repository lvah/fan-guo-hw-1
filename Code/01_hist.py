import pandas as pd
import plotly_express as px


def address_count_histograms(filename):
    # 1). load file and seperate by space
    df = pd.read_csv(filename, names=['operation', 'address'], sep=' ')
    # 2). transfer address to decimal_address
    df['decimal_address'] = df['address'].apply(lambda x: int(x, 16))
    # 3). plot and save histogram
    x_min = df['decimal_address'].min()
    x_max = df['decimal_address'].max()
    fig = px.histogram(df, x='decimal_address',
                       range_x=[x_min, x_max],
                       nbins=30,
                       title='%s Histogram' % (filename),
                       )
    fig.write_html('%s.html' % (filename.split('.')[0]))

if __name__ == '__main__':
    # filename = 'tex.din'
    filename = input("please input filename: ")
    address_count_histograms(filename)
