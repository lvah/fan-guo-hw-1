"""

"""

# import python module
import random
import time
import numpy as np
def generate_double_matrix(m=3, n=4):
    """
    Generate random int matrix, number range in[0,1)
    """
    matrix = np.random.random((m, n))
    matrix = matrix * random.randint(10, 100)
    return matrix
def multi_double_matrices():
    matrix1 = generate_double_matrix(m=300, n=400)
    matrix2 = generate_double_matrix(m=400, n=300)
    start = time.time()
    result = np.dot(matrix1, matrix2)
    end = time.time()
    return result, end - start
if __name__ == '__main__':
    result2, time2 = multi_double_matrices()
    print(time2)
